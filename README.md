# Simple Icons Brand Font

This repository contains The Simple Icons Brand Font. The font
is produced using [IcoMoon](https://icomoon.io/). You can rebuild the font by importing 
the file [Simple-Icons-Brands.json](https://gitlab.forge.hefr.ch/isc/simple-icons-brands/-/blob/master/Simple-Icons-Brands.json)
in IcoMoon.

**To see the font in action and for the list of all icons, look at the [demo page](https://isc.pages.forge.hefr.ch/simple-icons-brands).**

If you want to add more icons, just send me an [e-mail](mailto:jacques.supcik@hefr.ch) or send a _Merge Request_
with the updated `HEIAFR.json` file.